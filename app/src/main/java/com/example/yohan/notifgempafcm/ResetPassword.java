package com.example.yohan.notifgempafcm;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yohan.notifgempafcm.Helper.TextValidation;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;

public class ResetPassword extends AppCompatActivity {

    TextValidation txtVal = new TextValidation();
    TextView text;
    TextInputLayout newPassword, repeatPassword;
    Button btnReset;
    String emailReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        init();
    }

    void init(){
        emailReset = getIntent().getExtras().getString("email");
        text = findViewById(R.id.textPassword);
        text.setText("Username " + emailReset);
        newPassword = findViewById(R.id.inputNewPass);
        repeatPassword = findViewById(R.id.repeatNewPass);
        btnReset = findViewById(R.id.btnReset);
        btnReset.setOnClickListener(reset);
    }

    View.OnClickListener reset = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String newPass = newPassword.getEditText().getText().toString();
            String repeatPass = repeatPassword.getEditText().getText().toString();
            if (newPass.equals("") || repeatPass.equals("")){
                displayErrorIfEmpty();
            } else if (newPass.equals(repeatPass)){
                HashMap<String, String> data = pushData(emailReset, newPass);
                PostResponseAsyncTask taskReset = new PostResponseAsyncTask(ResetPassword.this, data, new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                        if (s.equals("1")){
                            Toast.makeText(getApplicationContext(), "Password berhasil diubah. Silahkan coba login", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            finish();
                            startActivity(i);
                        }
                    }
                });
                taskReset.execute(SplashActivity.URL + "/user/resetPassword");
            } else {
                Toast.makeText(getApplicationContext(), "Password tidak cocok. Silahkan coba lagi", Toast.LENGTH_SHORT).show();
                newPassword.getEditText().setText("");
                repeatPassword.getEditText().setText("");
            }
        }
    };

    public void displayErrorIfEmpty(){
        if (newPassword.getEditText().getText().toString().equals("")){
            txtVal.displayErrorIfEmpty(newPassword, "Ketikkan password Anda");
            txtVal.dismissErrorifTyped(newPassword);
        }
        if (repeatPassword.getEditText().getText().toString().equals("")){
            txtVal.displayErrorIfEmpty(repeatPassword, "Ketikkan ulang password Anda");
            txtVal.dismissErrorifTyped(repeatPassword);
        }
    }

    public HashMap<String, String> pushData(String email, String password){
        HashMap<String, String> data = new HashMap<>();
        data.put("email", email);
        data.put("password", password);
        return data;
    }
}
