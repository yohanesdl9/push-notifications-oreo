package com.example.yohan.notifgempafcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {
    private Activity activity = this;
    SharedPreferences sp;
    Map<String, ?> editor;
    public static final String URL = "http://192.168.43.154/info_gempa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sp = getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        editor = sp.getAll();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                HashMap<String, String> data = new HashMap<>();
                data.put("token", instanceIdResult.getToken());
                PostResponseAsyncTask insertToken = new PostResponseAsyncTask(SplashActivity.this, data, false, new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                    }
                });
                insertToken.execute(URL + "/notifikasi/putToken");
            }
        });
        Thread timer = new Thread(){
            public void run(){
                try {
                    sleep(5000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                } finally {
                    if (isNetworkConnected()){
                        Intent i;
                        if (sp.getBoolean("isLogin", false)){
                            i = new Intent(activity, MainActivity.class);
                        } else {
                            i = new Intent(activity, LoginActivity.class);
                        }
                        startActivity(i);
                        finish();
                    } else {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(activity).setTitle("Error")
                                        .setMessage("Sepertinya Anda sedang offline. Mohon menyambungkan ke internet kembali.")
                                        .setPositiveButton("Sambungkan Ulang", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent i = new Intent(activity, SplashActivity.class);
                                                finish();
                                                startActivity(i);
                                            }
                                        }).setNegativeButton("Keluar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                        moveTaskToBack(true);
                                    }
                                }).show();
                            }
                        });
                    }
                }
            }
        };
        timer.start();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)   getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null;
    }
}
