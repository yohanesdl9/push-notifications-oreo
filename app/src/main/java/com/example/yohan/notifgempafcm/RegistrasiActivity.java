package com.example.yohan.notifgempafcm;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yohan.notifgempafcm.Helper.TextValidation;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;

public class RegistrasiActivity extends AppCompatActivity {

    TextValidation txtVal = new TextValidation();
    TextInputLayout nameWrapper, emailWrapper, passwordWrapper, retypePasswordWrapper;
    Button btnRegistrasi;
    TextView txtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        init();
    }

    public void init(){
        nameWrapper = findViewById(R.id.txtLayoutNama);
        emailWrapper = findViewById(R.id.txtLayoutEmail);
        passwordWrapper = findViewById(R.id.txtLayoutPassword);
        retypePasswordWrapper = findViewById(R.id.txtLayoutUlangPassword);
        btnRegistrasi = findViewById(R.id.btnDaftar);
        txtLogin = findViewById(R.id.txtClickLogin);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        btnRegistrasi.setOnClickListener(registrasi);
    }

    public View.OnClickListener registrasi = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String nama = nameWrapper.getEditText().getText().toString();
            String email = emailWrapper.getEditText().getText().toString();
            String password = passwordWrapper.getEditText().getText().toString();
            String retypePassword = retypePasswordWrapper.getEditText().getText().toString();
            if (nama.equals("") || email.equals("") || password.equals("") || retypePassword.equals("")){
                displayErrorIfEmpty();
            } else {
                if (!txtVal.validateEmail(email)) {
                    emailWrapper.setError("Masukkan alamat email yang valid.");
                } else if (!password.equals(retypePassword)){
                    retypePasswordWrapper.setError("Password tidak cocok.");
                } else {
                    HashMap<String, String> data = pushData(nama, email, password);
                    PostResponseAsyncTask taskDaftar = new PostResponseAsyncTask(RegistrasiActivity.this, data, new AsyncResponse() {
                        @Override
                        public void processFinish(String s) {
                            Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                        }
                    });
                    taskDaftar.execute(SplashActivity.URL + "/user/registrasi");
                }
            }
        }
    };

    public void displayErrorIfEmpty(){
        if (nameWrapper.getEditText().getText().toString().equals("")) {
            txtVal.displayErrorIfEmpty(nameWrapper, "Masukkan nama lengkap Anda");
            txtVal.dismissErrorifTyped(nameWrapper);
        }
        if (emailWrapper.getEditText().getText().toString().equals("")) {
            txtVal.displayErrorIfEmpty(emailWrapper, "Masukkan email Anda yang valid");
            txtVal.dismissErrorifTyped(emailWrapper);
        }
        if (passwordWrapper.getEditText().getText().toString().equals("")) {
            txtVal.displayErrorIfEmpty(passwordWrapper, "Ketikkan password Anda");
            txtVal.dismissErrorifTyped(passwordWrapper);
        }
        if (retypePasswordWrapper.getEditText().getText().toString().equals("")) {
            txtVal.displayErrorIfEmpty(retypePasswordWrapper, "Ketikkan ulang password Anda");
            txtVal.dismissErrorifTyped(retypePasswordWrapper);
        }
    }

    public HashMap<String, String> pushData(String nama, String email, String password){
        HashMap<String, String> data = new HashMap<>();
        data.put("nama", nama);
        data.put("email", email);
        data.put("password", password);
        return data;
    }
}
