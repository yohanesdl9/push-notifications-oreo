package com.example.yohan.notifgempafcm.Models;

public class User {
    String email;
    String nama;
    String password;
    String prov_setting;
    String kota_setting;
    double last_loc_x;

    public String getProv_setting() {
        return prov_setting;
    }

    public void setProv_setting(String prov_setting) {
        this.prov_setting = prov_setting;
    }

    public String getKota_setting() {
        return kota_setting;
    }

    public void setKota_setting(String kota_setting) {
        this.kota_setting = kota_setting;
    }

    double last_loc_y;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getLast_loc_x() {
        return last_loc_x;
    }

    public void setLast_loc_x(double last_loc_x) {
        this.last_loc_x = last_loc_x;
    }

    public double getLast_loc_y() {
        return last_loc_y;
    }

    public void setLast_loc_y(double last_loc_y) {
        this.last_loc_y = last_loc_y;
    }
}
