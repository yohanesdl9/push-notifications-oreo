package com.example.yohan.notifgempafcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.yohan.notifgempafcm.Models.Kota;
import com.example.yohan.notifgempafcm.Models.Provinsi;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.yohan.notifgempafcm.Helper.Capitalize.capitalize;

public class SettingActivity extends AppCompatActivity {
    ArrayList<Provinsi> listProvinsi1;
    ArrayList<Kota> listKota1;
    ArrayList<String> listProvinsi = new ArrayList<>();
    ArrayList<String> listKota = new ArrayList<>();
    Spinner spinProvinsi, spinKota;
    TextInputLayout layoutNama, layoutEmail, layoutPassword;
    Button saveChanges;
    SharedPreferences sp;
    Map<String, ?> editor;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        sp = getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        editor = sp.getAll();
        edit = sp.edit();
        init();
    }

    public void init(){
        spinProvinsi = findViewById(R.id.spinProvinsi);
        spinKota = findViewById(R.id.spinKota);
        listProvinsi.add("- Pilih Provinsi -");
        loadSemuaProvinsi();
        layoutNama = findViewById(R.id.textLayoutNama);
        layoutEmail = findViewById(R.id.textLayoutEmail);
        layoutPassword = findViewById(R.id.textLayoutPassword);
        saveChanges = findViewById(R.id.btnSimpan);
        saveChanges.setOnClickListener(simpan);
        layoutNama.getEditText().setText(editor.get("nama").toString());
        layoutEmail.getEditText().setText(editor.get("email").toString());
    }

public View.OnClickListener simpan = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String old_email = editor.get("email").toString();
        String new_email = layoutEmail.getEditText().getText().toString();
        String name = layoutNama.getEditText().getText().toString();
        String kota_setting = spinKota.getSelectedItem().toString();
        String prov_setting = spinProvinsi.getSelectedItem().toString();
        String password = layoutPassword.getEditText().getText().toString();
        HashMap<String, String> data = pushData(old_email, new_email, name, kota_setting, prov_setting, password);
        if (name.equals("") || new_email.equals("")){
            Toast.makeText(getApplicationContext(), "Nama dan/atau email tidak boleh kosong", Toast.LENGTH_SHORT).show();
            layoutNama.getEditText().setText(editor.get("nama").toString());
            layoutEmail.getEditText().setText(editor.get("email").toString());
        } else {
            PostResponseAsyncTask saveSetting = new PostResponseAsyncTask(SettingActivity.this, data, new AsyncResponse() {
                @Override
                public void processFinish(String s) {
                    if (s.equals("1")) {
                        Toast.makeText(getApplicationContext(), "Pengaturan berhasil diubah.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            saveSetting.execute(SplashActivity.URL + "/user/saveChanges");
        }
    }
};

public HashMap<String, String> pushData(String old_email, String new_email, String name, String kota_setting, String prov_setting, String password){
    HashMap<String, String> data = new HashMap<>();
    data.put("new_email", new_email);
    data.put("name", name);
    data.put("kota_setting", kota_setting);
    data.put("prov_setting", prov_setting);
    if (old_email.equals(new_email)){
        data.put("old_email", old_email);
    }
    if (password.equals("")){
        data.put("password", password);
    }
    return data;
}

    public void loadSemuaProvinsi(){
        PostResponseAsyncTask taskProvinsi = new PostResponseAsyncTask(SettingActivity.this, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                listProvinsi1 = new JsonConverter<Provinsi>().toArrayList(s, Provinsi.class);
                for (int i = 0; i < listProvinsi1.size(); i++){
                    listProvinsi.add(capitalize(listProvinsi1.get(i).getNama_provinsi()));
                }
                ArrayAdapter<String> provAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, listProvinsi);
                provAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinProvinsi.setAdapter(provAdapter);
                spinProvinsi.setSelection(0);
            }
        });
        taskProvinsi.execute(SplashActivity.URL + "/provinsi");
        spinProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String prov_selected = parent.getItemAtPosition(position).toString();
                if (position != 0) {
                    loadSemuaKota(prov_selected);
                } else {
                    listKota.add("- Pilih Kota -");
                    ArrayAdapter<String> kotaAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, listKota);
                    kotaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinKota.setAdapter(kotaAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void loadSemuaKota(String provinsi){
        listKota.add("- Pilih Kota -");
        PostResponseAsyncTask taskKota = new PostResponseAsyncTask(SettingActivity.this, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                listKota1 = new JsonConverter<Kota>().toArrayList(s, Kota.class);
                for (int i = 0; i < listKota1.size(); i++){
                    listKota.add(capitalize(listKota1.get(i).getNama_kota()));
                }
                ArrayAdapter<String> kotaAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, listKota);
                kotaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinKota.setAdapter(kotaAdapter);
                spinKota.setSelection(0);
            }
        });
        taskKota.execute(SplashActivity.URL + "/kota/kota/" + provinsi.replace(" ", "_"));
    }
}
