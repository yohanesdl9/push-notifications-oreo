package com.example.yohan.notifgempafcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yohan.notifgempafcm.Helper.TextValidation;
import com.example.yohan.notifgempafcm.Models.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.ArrayList;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    ArrayList<User> userData = new ArrayList<>();
    SharedPreferences sp;
    public static final String MyPREFERENCE = "PREFERENCE";
    TextValidation txtVal = new TextValidation();
    TextInputLayout emailWrapper, passwordWrapper;
    Button btnLogin;
    TextView txtRegister, txtForgotPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sp = getSharedPreferences(MyPREFERENCE, Context.MODE_PRIVATE);
        init();
    }

    public void init(){
        emailWrapper = findViewById(R.id.txtEmail);
        passwordWrapper = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(validate);
        txtRegister = findViewById(R.id.txtClickDaftar);
        txtRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegistrasiActivity.class);
                startActivity(i);
            }
        });
        txtForgotPass = findViewById(R.id.txtClickForgot);
        txtForgotPass.setOnClickListener(forgotPassword);
    }

    public OnClickListener forgotPassword = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final EditText inputEmail = new EditText(v.getContext());
            inputEmail.setTextColor(Color.rgb(255, 255, 255));
            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            builder.setTitle("Masukkan Email Anda");
            builder.setMessage("Masukkan email yang Anda daftarkan : ").setView(inputEmail);
            builder.setNegativeButton("Batal", null).setPositiveButton("Selesai", null);
            final AlertDialog dialog = builder.create();
            dialog.show();
            Button btnSelesai = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            btnSelesai.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String emailReset = inputEmail.getText().toString();
                    if (!txtVal.validateEmail(emailReset) || emailReset.equals("")){
                        Toast.makeText(getApplicationContext(), "Masukkan alamat email yang valid", Toast.LENGTH_SHORT).show();
                    } else {
                        checkResetPass(emailReset, dialog);
                    }
                }
            });
        }
    };

    public void checkResetPass(final String emailReset, final AlertDialog dialog){
        HashMap<String, String> data = new HashMap<>();
        data.put("email", emailReset);
        PostResponseAsyncTask taskCheck = new PostResponseAsyncTask(LoginActivity.this, data, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                if (s.equals("1")) {
                    Intent i = new Intent(getApplicationContext(), ResetPassword.class);
                    i.putExtras(new Bundle());
                    i.putExtra("email", emailReset);
                    getApplicationContext().startActivity(i);
                    dialog.dismiss();
                } else {
                    Toast.makeText(getApplicationContext(), "Email yang dimasukkan tidak terdaftar di aplikasi", Toast.LENGTH_SHORT).show();
                }
            }
        });
        taskCheck.execute(SplashActivity.URL + "/user/checkUser");
    }

    public OnClickListener validate = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = emailWrapper.getEditText().getText().toString();
            String password = passwordWrapper.getEditText().getText().toString();
            if (email.equals("") || password.equals("")){
                displayErrorIfEmpty();
            } else {
                if (!txtVal.validateEmail(email)) {
                    emailWrapper.setError("Masukkan alamat email yang valid.");
                } else {
                    HashMap<String, String> data = pushData(email, password);
                    PostResponseAsyncTask taskLogin = new PostResponseAsyncTask(LoginActivity.this, data, new AsyncResponse() {
                        @Override
                        public void processFinish(String s) {
                            if (s.equals("1")){
                                setSessionData(email);
                            } else {
                                Toast.makeText(getApplicationContext(), "Email atau password tidak cocok. Silahkan coba lagi.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    taskLogin.execute(SplashActivity.URL + "/user/login");
                }
            }
        }
    };

    public void setSessionData(final String email){
        final SharedPreferences.Editor editor = sp.edit();
        HashMap<String, String> data = new HashMap<>();
        data.put("email", email);
        PostResponseAsyncTask taskLogin = new PostResponseAsyncTask(LoginActivity.this, data, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                System.out.println(s);
                userData = new JsonConverter<User>().toArrayList(s, User.class);
                editor.putString("nama", userData.get(0).getNama());
                editor.putString("email", userData.get(0).getEmail());
                editor.putString("kota", userData.get(0).getKota_setting());
                editor.putString("provinsi", userData.get(0).getProv_setting());
                editor.putString("lat", Double.toString(userData.get(0).getLast_loc_x()));
                editor.putString("lng", Double.toString(userData.get(0).getLast_loc_y()));
                editor.putBoolean("isLogin", true);
                editor.commit();
            }
        });
        taskLogin.execute(SplashActivity.URL + "/user/getUser");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                HashMap<String, String> sessData = new HashMap<>();
                sessData.put("token", instanceIdResult.getToken());
                sessData.put("email", email);
                PostResponseAsyncTask session = new PostResponseAsyncTask(LoginActivity.this, sessData, new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                        if (s.equals("1")){
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(getApplicationContext(), "Terjadi kesalahan yang tidak diketahui", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                session.execute(SplashActivity.URL + "/user/setSession");
            }
        });
    }

    public void displayErrorIfEmpty() {
        if (emailWrapper.getEditText().getText().toString().equals("")) {
            txtVal.displayErrorIfEmpty(emailWrapper, "Masukkan email Anda yang valid");
            txtVal.dismissErrorifTyped(emailWrapper);
        }
        if (passwordWrapper.getEditText().getText().toString().equals("")){
            txtVal.displayErrorIfEmpty(passwordWrapper, "Ketikkan password Anda");
            txtVal.dismissErrorifTyped(passwordWrapper);
        }
    }

    public HashMap<String, String> pushData(String email, String password){
        HashMap<String, String> data = new HashMap<>();
        data.put("email", email);
        data.put("password", password);
        return data;
    }
}
